// TODO: Retrieve (retrieve all and eager loading) and insert data from the database.
// TODO: 1. Install and load sequelize; install but do not load mysql
// TODO: 2. Set up MySQL connection
// TODO: 3. Define and load models
// TODO: 4. Persist registration information

// TODO: 9. Retrieve employee information based on queryString passed. Match queryString to both employee name
// TODO: 9. employee number
// TODO: 10. Perform eager loading so that the returned data includes the employee manager id and name

// DEPENDENCIES ------------------------------------------------------------------------------------------------------
// Loads express module and assigns it to a var called express
var express = require("express");

// Loads path to access helper functions for working with files and directory paths
var path = require("path");

// Loads bodyParser to populate and parse the body property of the request object
var bodyParser = require("body-parser");

// TODO: 1.1 Load sequelize, assign it to a variable called Sequelize
// Loads sequelize ORM

// CONSTANTS ---------------------------------------------------------------------------------------------------------
// Defines server port.
// Value of NODE_PORT is taken from the user environment if defined; port 3000 is used otherwise.
const NODE_PORT = process.env.NODE_PORT || 3000;

// Defines paths
// __dirname is a global that holds the directory name of the current module
const CLIENT_FOLDER = path.join(__dirname + '/../client');
const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages');

// TODO: 2.1 Define your MYSQL username and password as constants
// Defines MySQL configuration for username and password


// OTHER VARS ---------------------------------------------------------------------------------------------------------
//Create an instance of express application
var app = express();


// MIDDLEWARES --------------------------------------------------------------------------------------------------------
// Serves files from public directory (in this case CLIENT_FOLDER).
// __dirname is the absolute path of the application directory.
// if you have not defined a handler for "/" before this line, server will look for index.html in CLIENT_FOLDER
app.use(express.static(CLIENT_FOLDER));

// Populates req.body with information submitted through the registration form.
// Expected content type is application/x-www-form-urlencoded
//app.use(bodyParser.urlencoded({extended: false}));
// Default $http content type is application/json so we use json as the parser type
app.use(bodyParser.json());


// DBs, MODELS, and ASSOCIATIONS ---------------------------------------------------------------------------------------
//TODO :2.2 Create a connection to MySQL DB
// Creates a MySQL connection


// TODO: 3.3 Load employees table model

// TODO: 9.1 Load departments and dept_emp table models

// TODO: 9.2 Define associations



// ROUTE HANDLERS -----------------------------------------------------------------------------------------------------
// Defines endpoint exposed to client side for registration
app.post("/api/employees", function (req, res, next) {
    // Information sent via an HTTP POST is found in req.body
    console.log('\nInformation submitted to server:')
    console.log(req.body);

    // TODO: 4.1 Persist (save) registration information using sequelize

});

// TODO: 9.3 Retrieve the first 100 employee records that match query string passed. Match against emp name and emp no
// Defines endpoint handler exposed to client side for retrieving employee information from database. Client side
// sent data as part of the query string, we access query string paramters via the req.query property
app.get("/api/employees", function (req, res) {

});


/* TODO: 9.4 Retrieve the first 100 employee records that match query string passed. Match against employee name and
 employee no. Include  department information */

/* Defines endpoint handler exposed to client side for retrieving employee records that match query string passed.
 Match against dept name and dept no. Includes manager information. Client side sent data as part of the query
 string, we access query string paramters via the req.query property
*/
app.get("/api/employees/departments", function (req, res) {

});


// Defines endpoint exposed to client side for retrieving all employee information (STATIC)
app.get("/api/static/employees", function (req, res) {
    var employees = [
        {
            empNo: 1001,
            empFirstName: 'Emily',
            empLastName: 'Smith',
            empPhoneNumber: '6516 2093'

        }
        , {
            empNo: 1002,
            empFirstName: 'Varsha',
            empLastName: 'Jansen',
            empPhoneNumber: '6516 2093'
        }
        ,
        {
            empNo: 1003,
            empFirstName: 'Julie',
            empLastName: 'Black',
            empPhoneNumber: '6516 2093'
        }
        , {
            empNo: 1004,
            empFirstName: 'Fara',
            empLastName: 'Johnson',
            empPhoneNumber: '6516 2093'
        }
        ,
        {
            empNo: 1005,
            empFirstName: 'Justin',
            empLastName: 'Zhang',
            empPhoneNumber: '6516 2093'
        }
        , {
            empNo: 1006,
            empFirstName: 'Kenneth',
            empLastName: 'Black',
            empPhoneNumber: '6516 2093'
        }

    ];
    // TODO: 10.3 Return the employees information as a json object
    // Return employees as a json object
    res
        .status(200)
        .json(employees);
});


// Handles 404. In Express, 404 responses are not the result of an error,
// so the error-handler middleware will not capture them.
// To handle a 404 response, add a middleware function at the very bottom of the stack
// (below all other path handlers)
app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

// Error handler: server error
app.use(function (err, req, res, next) {
    res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));
});

// Server starts and listens on NODE_PORT
app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT);
});