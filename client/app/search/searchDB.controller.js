// TODO: Retrieve (retrieve all and eager loading) and insert data from the database.-->
/* TODO: 8. Create searchDB controller that supports feature of the searchDB partial. Check searchDB.html to know what
 the requirements are
*/
(function () {
    angular
        .module()
        .controller();

    SearchDBCtrl.$inject = [];

    function SearchDBCtrl(EmpService) {
        var vm = this;

        vm.searchString = '';
        vm.result = null;
        vm.showDepartment = false;

        // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view.


        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        // init is a private function (i.e., not exposed)
        init();

        // Function declaration and definition -------------------------------------------------------------------------
        // The init function initializes view
        function init() {
            // We call EmpService.retrieveEmpDB to handle retrieval of department information. The data retrieved
            // from this function is used to populate search.html. Since we are initializing the view, we want to
            // display all available departments, thus we ask service to retrieve '' (i.e., match all)

        }

        // The search function searches for employees that match query string entered by user. The query string is
        // matched against the employee first name, last name, and employee number.
        function search() {
            vm.showDepartment = false;

        }

        // The search function searches for departments that matches query string entered by user. The query string is
        // matched against the employee name and employee number alike.
        function searchForDepartment() {
            vm.showDepartment = true;

        }
    }
})();